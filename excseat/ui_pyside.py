'''
Created on Jun 15, 2013

@author: afg
'''

import os
import sys
import math
import configparser
import traceback
import PySide
from PySide.QtGui import *
from PySide.QtCore import *
from . import core
from . import strutil
from .osext import defaultOpen

__version__ = '2.4.1'

class application:
    appname = '換座位'
    author = 'afg'
    apptitle = appname
    icon = 'ICONIMAGE'
    userdata = 'userdata'
    configfile = os.path.join(userdata, 'config.txt')
    namefile = os.path.join(userdata, 'namelist.txt')
    saved = os.path.join(userdata, 'saved_seats')

class CustomConfigParser():
    def __init__(self, section, file=application.configfile, encoding=sys.getdefaultencoding()):
        self.filename = file
        self.section = section
        self.config = configparser.ConfigParser()
        self.encoding = encoding
        
    def load(self, *args, writeback=False):
        '''\
        arg format:
            (key, default, type-check)
            type-check can be int, float, bool or None
        '''
        if self.section not in self.config.sections():
            self.config.add_section(self.section)
        self.config.read(self.filename, encoding=self.encoding)
        loaded = list()
        for option, default, typecheck in args:
            kwds = dict(section=self.section, option=option, fallback=default)
            try:
                if typecheck is int:
                    got = self.config.getint(**kwds)
                elif typecheck is float:
                    got = self.config.getfloat(**kwds)
                elif typecheck is bool:
                    got = self.config.getboolean(**kwds)
                elif typecheck is None:
                    got = self.config.get(**kwds)
                else:
                    raise TypeError('{} = {:r} failed type-check: {:r}'.format(option, got, typecheck))
            except ValueError:
                got = default
            if writeback and (typecheck is None or isinstance(got, typecheck)):
                self.config.set(self.section, option, str(got))
            loaded.append(got)
        return loaded
    
    def dump(self, **to_update):
        if self.section not in self.config.sections():
            self.config.add_section(self.section)
        self.config[self.section].update({key: str(value) for (key, value) in to_update.items()})
        try:
            with open(self.filename, mode='w', encoding=self.encoding) as conffile:
                self.config.write(conffile)
        except Exception:
            traceback.print_exc()
            

class Configure(QDialog):
    SKIP_NUM_SEP = ' '
    SKIP_NUM_RANGE = '-'
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle(application.apptitle)
        self.setWindowFlags((self.windowFlags() | Qt.MSWindowsFixedSizeDialogHint) & ~Qt.WindowContextHelpButtonHint)
        self.cfparser = CustomConfigParser('Table')
        self.createWidgets()
        self.setupLayout()
        self.startNumberEdit.selectAll()
        
    def createWidgets(self):
        self.peopleCount = QLabel(alignment=Qt.AlignCenter)
        self.startNumberEdit = QLineEdit(alignment=Qt.AlignCenter)
        self.endNumberEdit = QLineEdit(alignment=Qt.AlignCenter)
        self.skipNumbersEdit = QLineEdit(alignment=Qt.AlignCenter)
        self.colCountEdit = QLineEdit(alignment=Qt.AlignCenter)
        self.rowCountEdit = QLineEdit(alignment=Qt.AlignCenter)
        
        self.startNumberEdit.setValidator(QIntValidator(bottom=0))
        self.endNumberEdit.setValidator(QIntValidator(bottom=0))
        self.rowCountEdit.setValidator(QIntValidator(bottom=0))
        self.colCountEdit.setValidator(QIntValidator(bottom=0))
        
        self.continueButton = QPushButton('繼續')
        self.buttonBox = QDialogButtonBox()
        self.buttonBox.addButton(self.continueButton, self.buttonBox.ActionRole)
        
        self.startNumberEdit.textChanged.connect(self.meth_startNumEdit_textChanged)
        self.endNumberEdit.textChanged.connect(self.autoUpdates)
        self.skipNumbersEdit.textEdited.connect(self.autoUpdates)
        self.colCountEdit.textEdited.connect(self.autoUpdateRowCount)
        self.continueButton.clicked.connect(self.forward)
        
        self.config_load()
        self.autoUpdatePeopleCount()
        
    def setupLayout(self):
        layout = QFormLayout()
        layout.addRow('總人數', self.peopleCount)
        layout.addRow('起始座號', self.startNumberEdit)
        layout.addRow('結束座號', self.endNumberEdit)
        layout.addRow('空號', self.skipNumbersEdit)
        layout.addRow('座位行數', self.colCountEdit)
        layout.addRow('座位列數', self.rowCountEdit)
        layout.addRow(self.buttonBox)
        self.setLayout(layout)
        
    def meth_startNumEdit_textChanged(self):
        if self.startNumberEdit.text():
            startNum = int(self.startNumberEdit.text())
            self.endNumberEdit.setValidator(QIntValidator(bottom=startNum))
            try:
                if int(self.endNumberEdit.text()) < startNum:
                    self.endNumberEdit.setText(str(startNum))
            except ValueError:
                pass
        self.autoUpdates()
        
    def config_load(self):
        start, end = self.cfparser.load(('number_start', 1, int),
                                        ('number_end', '', int))
        self.startNumberEdit.setText(str(start))
        self.endNumberEdit.setText(str(end))
        default = ''
        self.skipNumbersEdit.setText(self.cfparser.load(('numbers_skipped', default, None))[0])
        try:
            self.getSkippedPeopleSet()
        except:
            self.skipNumbersEdit.setText(default)
        else:
            if not self.skipNumbersEdit.text():
                self.skipNumbersEdit.setText(default)
        rows, cols = self.cfparser.load(('rows', '', int),
                                        ('columns', 7, int))
        self.rowCountEdit.setText(str(rows))
        self.colCountEdit.setText(str(cols))
            
    def config_dump(self):
        self.cfparser.dump(number_start=int(self.startNumberEdit.text()),
                           number_end=int(self.endNumberEdit.text()),
                           numbers_skipped=self.skipNumbersEdit.text(),
                           rows=int(self.rowCountEdit.text()),
                           columns=int(self.colCountEdit.text()))
    
    def autoUpdates(self):
        self.autoUpdatePeopleCount()
        self.autoUpdateRowCount()
    
    def autoUpdatePeopleCount(self):
        count = self.getPeopleCount()
        if count is None:
            count = 'N/A'
        self.peopleCount.setText(str(count))
    
    def getPeopleCount(self):
        try:
            start = int(self.startNumberEdit.text())
            end = int(self.endNumberEdit.text())
            skips = self.getSkippedPeopleSet()
        except ValueError:
            count = 'N/A'
        else:
            count = max(0, end + 1 - start) - len([x for x in skips if (start <= x <= end)])
        return count
    
    def autoUpdateRowCount(self):
        if not self.rowCountEdit.isModified() and self.colCountEdit.text():
            try:
                self.rowCountEdit.setText(str(math.ceil(self.getPeopleCount()/int(self.colCountEdit.text()))))
            except (ValueError, ZeroDivisionError, TypeError):
                pass  
    
    def getPeopleList(self):
        return sorted(set(range(int(self.startNumberEdit.text()), int(self.endNumberEdit.text())+1))-self.getSkippedPeopleSet())
    
    def getSkippedPeopleSet(self):
        string = self.skipNumbersEdit.text()
        skips = set()
        for part in string.split(self.SKIP_NUM_SEP):
            if part:
                try:
                    head, tail = part.split(self.SKIP_NUM_RANGE, 1)
                except ValueError:
                    skips.add(int(part))
                else:
                    if head and tail:
                        skips.update(set(range(int(head), int(tail)+1)))
                    else:
                        skips.add(int(head) if head else int(tail))
        return skips
    
    def forward(self):
        try:
            cols = int(self.colCountEdit.text())
            rows = int(self.rowCountEdit.text())
            nums = self.getPeopleList()
            core.Table(rows, cols, nums)
        except Exception:
            detailed = None
            if not self.startNumberEdit.text():
                text = '起始座號不可空白'
            elif not self.endNumberEdit.text():
                text = '結束座號不可空白'
            elif not self.getPeopleCount():
                text = '總人數不可為零'
            elif not self.rowCountEdit.text():
                text = '座位列數不可空白'
            elif not self.colCountEdit.text():
                text = '座位行數不可空白'
            elif not int(self.rowCountEdit.text()):
                text = '座位列數不可為零'
            elif not int(self.colCountEdit.text()):
                text = '座位行數不可為零'
            elif self.getPeopleCount() > int(self.rowCountEdit.text()) * int(self.colCountEdit.text()):
                text = '總人數大於座位數'
            else:
                text = traceback.format_exc().splitlines()[-1]
                detailed = traceback.format_exc()
            msgbox_kwargs = dict(windowTitle='錯誤',
                            text=text,
                            detailedText=detailed,
                            icon=QMessageBox.Warning)
            QMessageBox(self, **msgbox_kwargs).exec_()
        else:
            self.config_dump()
            mainWindow = MainWindow(rows=rows, cols=cols, nums=nums)
            self.hide()
            mainWindow.exec_()
            if mainWindow.result() == mainWindow.Accepted:
                self.show()
            else:
                self.close()

class SeatButton(QPushButton):
    PreferredWidth = 50
    def __init__(self, location, inTable, parent=None):
        super().__init__(parent=None)
        self.location = location
        self.inTable = inTable
        self.track = inTable.table.getSeat(*location)
    
    def getName(self):
        return '{}{}'.format(strutil.int_to_chars(self.location[0]+1), self.location[1]+1)
    
    def uiUpdate(self):
        if self.track is not None:
            self.setDisabled(self.track.isDisabled())
            self.setText(str(self.track))
            
    def sizeHint(self):
        return QSize(self.PreferredWidth, super().sizeHint().height())
    
    def meth_clicked(self):
        self.inTable.meth_seatButton_clicked(*self.location)

class MWUndosDialog(QDialog):
    def __init__(self, maxSteps, parent):
        super().__init__(parent=parent)
        self.setWindowTitle('多次復原')
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.maxSteps = maxSteps
        self.undoSteps = 0
        self.justReset = False
        self.createWidgets()
        
    def createWidgets(self):
        self.QuestionLabel = QLabel('要復原多少步驟？（{}~{}）'.format(0, self.maxSteps))
        
        self.undoStepsEdit = QLineEdit('0', alignment=Qt.AlignCenter, maximumWidth=60)
        self.undoStepsSlider = QSlider(Qt.Horizontal)
        undoStepsLayout = QHBoxLayout()
        undoStepsLayout.addWidget(self.undoStepsEdit)
        undoStepsLayout.addWidget(self.undoStepsSlider)
        
        self.justResetButton = QPushButton('乾脆重設')
        self.confirmUndoButton = QPushButton('復原')
        self.buttonBox = QDialogButtonBox()
        self.buttonBox.addButton(self.justResetButton, self.buttonBox.ResetRole)
        self.buttonBox.addButton(self.confirmUndoButton, self.buttonBox.AcceptRole)
        
        self.undoStepsSlider.setRange(0, self.maxSteps)
        self.undoStepsSlider.setValue(0)
        self.undoStepsSlider.valueChanged.connect(self.meth_undoStepsSlider_valueChanged)
        self.undoStepsEdit.setValidator(QIntValidator(bottom=0, top=self.maxSteps))
        self.undoStepsEdit.textEdited.connect(self.meth_undoStepsEdit_edited)
        self.justResetButton.clicked.connect(self.meth_jsutResetButton_clicked)
        self.confirmUndoButton.clicked.connect(self.accept)
        
        layout = QVBoxLayout()
        layout.addWidget(self.QuestionLabel)
        layout.addLayout(undoStepsLayout)
        layout.addWidget(self.buttonBox)
        
        self.setLayout(layout)
    
    def meth_jsutResetButton_clicked(self):
        self.justReset = True
        self.accept()
        
    def meth_undoStepsEdit_edited(self):
        self.undoStepsSlider.setValue(int(self.undoStepsEdit.text()))
        self.updateUndoSteps()
        
    def meth_undoStepsSlider_valueChanged(self):
        self.undoStepsEdit.setText(str(self.undoStepsSlider.value()))
        self.updateUndoSteps()
        
    def updateUndoSteps(self):
        try:
            self.undoSteps = int(self.undoStepsEdit.text())
        except ValueError:
            self.undoSteps = 0
        self.confirmUndoButton.setText('復原({})'.format(self.undoSteps))
        

class MWSaveOptionsDialog(QDialog):
    def __init__(self, parent):
        super().__init__(parent=parent)
        self.setWindowTitle('存檔選項')
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.createWidgets()
        self.load_from_parent()
        self.updateDynamicLayout()
        
    def createWidgets(self):
        self.outputTypeBox = QGroupBox('輸出類型')
        self.asHtml = QRadioButton('網頁(*.html)')
        self.asTxt = QRadioButton('純文字(*.txt)')
        
        self.alignmentBox = QGroupBox('文字對齊方式')
        self.centerRadio = QRadioButton('置中')
        self.ljustRadio = QRadioButton('靠左')
        self.rjustRadio = QRadioButton('靠右')
        self.showLineNumbers = QCheckBox('顯示行列編號')
        self.showNames = QCheckBox('依 "namelist.txt" 產生對應姓名')
        
        spbconf = dict(alignment=Qt.AlignCenter, maximum=999)
        self.htmlBox = QGroupBox('網頁屬性')
        self.html_seatColor = QComboBox()
        self.html_border = QSpinBox(suffix='px', **spbconf)
        self.html_vpadding = QSpinBox(suffix='px', **spbconf)
        self.html_hpadding = QSpinBox(suffix='px', **spbconf)
        self.html_font_size = QSpinBox(suffix='px', **spbconf)
        self.txtBox = QGroupBox('文字輸出設定')
        self.txt_borderless = QCheckBox('無框線')
        self.txt_sameCellSize = QCheckBox('大小相同的表格')
        self.txt_vexpand = QSpinBox(suffix=' lines', **spbconf)
        self.txt_hexpand = QSpinBox(suffix=' spaces', **spbconf)
        
        self.buttonBox = QDialogButtonBox()
        self.confirmButton = self.buttonBox.addButton('確定', self.buttonBox.AcceptRole)
        self.toSaveButton = self.buttonBox.addButton('儲存座位', self.buttonBox.ApplyRole)
        
        for color in ['DimGray', 'DarkOrange', 'ForestGreen', 'RoyalBlue', 'SlateBlue']:
            self.html_seatColor.addItem(color)
        self.html_seatColor.setEditable(True)
        
        self.asHtml.clicked.connect(self.updateDynamicLayout)
        self.asTxt.clicked.connect(self.updateDynamicLayout)
        self.confirmButton.clicked.connect(self.meth_confirmButton_clicked)
        self.toSaveButton.clicked.connect(self.meth_toSaveButton_clicked)
        
        outputTypeLayout = QHBoxLayout()
        outputTypeLayout.addWidget(self.asHtml)
        outputTypeLayout.addWidget(self.asTxt)
        self.outputTypeBox.setLayout(outputTypeLayout)
        alignLayout = QHBoxLayout()
        alignLayout.addWidget(self.ljustRadio)
        alignLayout.addWidget(self.centerRadio)
        alignLayout.addWidget(self.rjustRadio)
        self.alignmentBox.setLayout(alignLayout)
        
        htmlLayout = QFormLayout()
        htmlLayout.addRow('垂直留白', self.html_vpadding)
        htmlLayout.addRow('水平留白', self.html_hpadding)
        htmlLayout.addRow('表格邊框', self.html_border)
        htmlLayout.addRow('文字大小', self.html_font_size)
        htmlLayout.addRow('顏色', self.html_seatColor)
        self.htmlBox.setLayout(htmlLayout)
        txtLayout = QFormLayout()
        txtLayout.addRow('垂直留白', self.txt_vexpand)
        txtLayout.addRow('水平留白', self.txt_hexpand)
        txtLayout.addRow(self.txt_borderless)
        txtLayout.addRow(self.txt_sameCellSize)
        self.txtBox.setLayout(txtLayout)
        self.dynamicLayout = QStackedLayout()
        self.dynamicLayout.addWidget(self.htmlBox)
        self.dynamicLayout.addWidget(self.txtBox)
        
        layout = QGridLayout()
        layout.addWidget(self.outputTypeBox, 0, 0)
        layout.addLayout(self.dynamicLayout, 0, 1, 4, 1)
        layout.addWidget(self.alignmentBox, 1, 0)
        layout.addWidget(self.showLineNumbers, 2, 0)
        layout.addWidget(self.showNames, 3, 0)
        layout.addWidget(self.buttonBox, 4, 1, 1, 2)
        self.setLayout(layout)
    
    def updateDynamicLayout(self):
        if self.asHtml.isChecked():
            self.dynamicLayout.setCurrentIndex(0)
        elif self.asTxt.isChecked():
            self.dynamicLayout.setCurrentIndex(1)
    
    def meth_confirmButton_clicked(self):
        self.dump_to_parent()
        self.close()
        
    def meth_toSaveButton_clicked(self):
        self.meth_confirmButton_clicked()
        self.parent().meth_saveButton_clicked()
        
    def load_from_parent(self):
        try:
            p = self.parent().cfparser.config[self.parent().cfparser.section]
            if p.getboolean('save_as_html'):
                self.asHtml.toggle()
            else:
                self.asTxt.toggle()
            align = p.get('alignment')
            if align is strutil.LJUST:
                self.ljustRadio.toggle()
            elif align is strutil.RJUST:
                self.rjustRadio.toggle()
            else:
                self.centerRadio.toggle()
            self.showLineNumbers.setChecked(p.getboolean('show_line_numbers'))
            self.showNames.setChecked(p.getboolean('show_names'))
            self.txt_vexpand.setValue(p.getint('txt_vexpand'))
            self.txt_hexpand.setValue(p.getint('txt_hexpand'))
            self.txt_borderless.setChecked(p.getboolean('txt_borderless'))
            self.txt_sameCellSize.setChecked(p.getboolean('txt_same_cell_size'))
            self.html_vpadding.setValue(p.getint('html_vpadding'))
            self.html_hpadding.setValue(p.getint('html_hpadding'))
            self.html_border.setValue(p.getint('html_border'))
            self.html_font_size.setValue(p.getint('html_font_size'))
            self.html_seatColor.setEditText(p.get('html_seat_color'))
        except Exception:
            traceback.print_exc()
        
    def dump_to_parent(self):
        try:
            if self.ljustRadio.isChecked():
                align = strutil.LJUST
            elif self.rjustRadio.isChecked():
                align = strutil.RJUST
            elif self.centerRadio.isChecked():
                align = strutil.CENTER
            self.parent().cfparser.dump(alignment=align,
                                        show_line_numbers=self.showLineNumbers.isChecked(),
                                        show_names=self.showNames.isChecked(),
                                        save_as_html=self.asHtml.isChecked(),
                                        txt_vexpand=self.txt_vexpand.value(),
                                        txt_hexpand=self.txt_hexpand.value(),
                                        txt_borderless=self.txt_borderless.isChecked(),
                                        txt_same_cell_size=self.txt_sameCellSize.isChecked(),
                                        html_vpadding=self.html_vpadding.value(),
                                        html_hpadding=self.html_hpadding.value(),
                                        html_border=self.html_border.value(),
                                        html_font_size=self.html_font_size.value(),
                                        html_seat_color=self.html_seatColor.currentText()
                                        )
        except Exception:
            traceback.print_exc()

class MainWindow(QDialog):
    def __init__(self, rows, cols, nums, parent=None):
        super().__init__(parent=parent)
        self.table = core.Table(rows, cols, nums)
        self._rows = rows
        self._cols = cols
        self._nums = nums
        self.cfparser = CustomConfigParser('Save Options')
        self.config_load()
        self.setWindowTitle(application.apptitle)
        self.setWindowFlags(self.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.createWidgets()
        self.uiUpdate()
        
    def createWidgets(self):
        # Action Tab
        # SeatTable
        seatTableGridLayout = QGridLayout()
        
        self.seatButtons = list()
        for col in range(self._cols):
            seatTableGridLayout.addWidget(QLabel(strutil.int_to_chars(col+1), alignment=Qt.AlignCenter), 0, col+1)
        for row in range(self._rows):
            seatTableGridLayout.addWidget(QLabel(str(row+1), alignment=Qt.AlignCenter), row+1, 0)
        for rownum in range(self._rows):
            self.seatButtons.append(list())
            for colnum in range(self._cols):
                sb = SeatButton(location=(rownum, colnum),
                                inTable=self)
                sb.clicked.connect(sb.meth_clicked)
                seatTableGridLayout.addWidget(sb, rownum+1, colnum+1)
                self.seatButtons[-1].append(sb)
        
        self.insHighlightStyle = 'color:blue'
        self.insHiddenStyle = 'color:grey'
        self.insStep1Label = QLabel(alignment=Qt.AlignCenter)
        self.insSep1 = QLabel('>>', alignment=Qt.AlignCenter)
        self.insStep2Label = QLabel(alignment=Qt.AlignCenter)
        self.insSep2 = QLabel('>>', alignment=Qt.AlignCenter)
        self.insStep3Label = QLabel('儲存座位', alignment=Qt.AlignCenter)
        self.insSep1.setStyleSheet(self.insHiddenStyle)
        self.insSep2.setStyleSheet(self.insHiddenStyle)
        instructionLayout = QHBoxLayout()
        instructionLayout.addWidget(self.insStep1Label)
        instructionLayout.addWidget(self.insSep1)
        instructionLayout.addWidget(self.insStep2Label)
        instructionLayout.addWidget(self.insSep2)
        instructionLayout.addWidget(self.insStep3Label)
        
        self.queueList = [QLabel(alignment=Qt.AlignCenter) for x in range(int(max(5, self._cols*1.2)))]
        queueListLayout = QHBoxLayout()
        for x in self.queueList:
            queueListLayout.addWidget(x, alignment=Qt.AlignCenter)
        instructionGroupBoxLayout = QVBoxLayout()
        instructionGroupBoxLayout.addLayout(instructionLayout)
        instructionGroupBoxLayout.addLayout(queueListLayout)
        self.instructionGroupBox = QGroupBox('操作流程指示')
        self.instructionGroupBox.setLayout(instructionGroupBoxLayout)
        
        self.dynamicButtonBox = QStackedLayout()
        self.staticButtonBox = QDialogButtonBox()
        self.dbb_disable = QDialogButtonBox()
        self.dbb_choose = QDialogButtonBox()
        self.dbb_save = QDialogButtonBox()
        self.dynamicButtonBox.addWidget(self.dbb_disable)
        self.dynamicButtonBox.addWidget(self.dbb_choose)
        self.dynamicButtonBox.addWidget(self.dbb_save)
        
        self.undoButton = self.staticButtonBox.addButton('復原', QDialogButtonBox.ResetRole)
        self.postponeButton = self.dbb_choose.addButton('晚點再選', QDialogButtonBox.ApplyRole)
        self.randomButton = self.dbb_choose.addButton('隨機選擇', QDialogButtonBox.ApplyRole)
        self.undosButton = self.staticButtonBox.addButton('多次復原...', QDialogButtonBox.ResetRole)
        self.skipDisableButton = self.dbb_disable.addButton('跳過此步', QDialogButtonBox.ApplyRole)
        self.randomAllButton = self.dbb_choose.addButton('全部隨機', QDialogButtonBox.ApplyRole)
        self.aboutButton = self.staticButtonBox.addButton('關於...', QDialogButtonBox.ApplyRole)
        self.returnButton = self.staticButtonBox.addButton('回設定頁', QDialogButtonBox.DestructiveRole)
        self.saveButton = self.dbb_save.addButton('儲存座位', QDialogButtonBox.ApplyRole)
        self.saveOptions = self.dbb_save.addButton('存檔選項...', QDialogButtonBox.ApplyRole)
        
        self.undoButton.clicked.connect(self.meth_undoButton_clicked)
        self.undosButton.clicked.connect(self.meth_undosButton_clicked)
        self.randomButton.clicked.connect(self.meth_randomButton_clicked)
        self.randomAllButton.clicked.connect(self.meth_randomAllButton_clicked)
        self.postponeButton.clicked.connect(self.meth_postponeButton_clicked)
        self.skipDisableButton.clicked.connect(self.meth_skipDisableButton_clicked)
        self.aboutButton.clicked.connect(self.meth_aboutButton_clicked)
        self.returnButton.clicked.connect(self.meth_returnButton_clicked)
        self.saveButton.clicked.connect(self.meth_saveButton_clicked)
        self.saveOptions.clicked.connect(self.meth_saveOptions_clicked)
        
        actionLayout = QVBoxLayout()
        actionLayout.addLayout(seatTableGridLayout)
        actionLayout.addWidget(self.instructionGroupBox)
        actionLayout.addLayout(self.dynamicButtonBox)
        actionLayout.addWidget(self.staticButtonBox)
        
        self.setLayout(actionLayout)
        
    def resetAll(self):
        self.table.reset()
        self.uiUpdate()
        
    def uiUpdate(self):
        # means not saved
        self.seatIsSaved = False
        # update seat buttons
        for row in self.seatButtons:
            for sb in row:
                sb.uiUpdate()
        # update instructions
        self.insStep1Label.setText('移除多餘的座位'+(' ({})'.format(len(self.table.getAvailable())-len(self._nums))
                                   if self.table.getStatus() is self.table.STATUS_DISABLE else ''))
        self.insStep2Label.setText('選擇座位'+(' ({})'.format(len(self.table._queue))
                                   if self.table.getStatus() is self.table.STATUS_CHOOSE else ''))
        toHidden = [self.insStep1Label, self.insStep2Label, self.insStep3Label]
        for index, status in enumerate([self.table.STATUS_DISABLE, self.table.STATUS_CHOOSE, self.table.STATUS_DONE]):
            if self.table.getStatus() is status:
                toHidden.pop(index).setStyleSheet(self.insHighlightStyle)
                break
        for widget in toHidden:
            widget.setStyleSheet(self.insHiddenStyle)
            # update queue list
        queueListFromTable = self.table.getQueue() 
        for index, quobj in enumerate(self.queueList):
            try:
                gotQueue = str(queueListFromTable[index])
            except IndexError:
                if index:
                    quobj.setText('')
                else:
                    quobj.setText('(done)')
                    quobj.setStyleSheet(self.insHiddenStyle)
            else:
                quobj.setText(gotQueue)
                if self.table.getStatus() is self.table.STATUS_CHOOSE:
                    if not index:
                        quobj.setStyleSheet(self.insHighlightStyle)
                else:
                    quobj.setStyleSheet(self.insHiddenStyle)
        # update action buttons
        self.dynamicButtonBox.setCurrentIndex([self.table.STATUS_DISABLE,
                                               self.table.STATUS_CHOOSE,
                                               self.table.STATUS_DONE].index(self.table.getStatus()))
        self.undoButton.setDisabled(not self.table._undos)
        self.undosButton.setDisabled(not self.table._undos)
    
    def closeEvent(self, event):
        if self.seatIsSaved or not self.table._undos:
            event.accept()
        else:
            msgbox = QMessageBox(self)
            msgbox.setWindowTitle(application.apptitle)
            msgbox.setIcon(msgbox.Question)
            msgbox.setText('確定要離開嗎？\n\n所有未儲存的資料將會遺失')
            yes = msgbox.addButton('是', msgbox.AcceptRole)
            msgbox.addButton('否', msgbox.RejectRole)
            msgbox.exec_()
            if msgbox.clickedButton() == yes:
                event.accept()
            else:
                event.ignore()
    
    def rmdhdcfg(self, head=None, ignore=[], cot=False):
        ret = dict()
        p = self.cfparser.config[self.cfparser.section]
        for key in p:
            if not any(key.startswith(x) for x in list(ignore)):
                if cot:
                    try:
                        value = p.getboolean(key)
                    except ValueError:
                        try:
                            value = p.getint(key)
                        except ValueError:
                            value = p.get(key)
                else:
                    value = p.get(key)
                ret[key[len(head):] if key.startswith(head) else key] = value
        return ret
    
    def config_load(self):
        self.cfparser.load(
            ('alignment', strutil.CENTER, None),
            ('show_line_numbers', True, bool),
            ('show_names', False, bool),
            ('save_as_html', True, bool),
            ('txt_vexpand', 0, int),
            ('txt_hexpand', 2, int),
            ('txt_borderless', False, bool),
            ('txt_same_cell_size', False, bool),
            ('html_vpadding', 6, int),
            ('html_hpadding', 12, int),
            ('html_border', 3, int),
            ('html_font_size', 18, int),
            ('html_seat_color', 'DimGray', None),
            writeback=True
        )
    
    def meth_seatButton_clicked(self, row, col):
        self.table.action_toggle(row, col)
        self.uiUpdate()
        
    def meth_undoButton_clicked(self):
        self.table.action_undo()
        self.uiUpdate()
        
    def meth_undosButton_clicked(self):
        m = MWUndosDialog(maxSteps=len(self.table._undos),
                        parent=self)
        m.exec_()
        if m.result() == m.Accepted:
            if m.justReset:
                self.resetAll()
            else:
                for x in range(m.undoSteps):
                    self.table.action_undo()
                self.uiUpdate()
    
    def meth_randomButton_clicked(self):
        self.table.action_random()
        self.uiUpdate()
    
    def meth_randomAllButton_clicked(self):
        self.table.action_random_all()
        self.uiUpdate()
    
    def meth_postponeButton_clicked(self):
        self.table.action_postpone()
        self.uiUpdate()
    
    def meth_skipDisableButton_clicked(self):
        self.table.action_skip_disable()
        self.uiUpdate()
        
    def meth_returnButton_clicked(self):
        if self.close():
            self.accept()
    
    def meth_saveButton_clicked(self):
        html = self.cfparser.config.getboolean(self.cfparser.section, 'save_as_html')
        filename, filter = QFileDialog.getSaveFileName(parent=self,
                                                       dir=application.saved,
                                                       caption='儲存座位',
                                                       filter='Webpage (*.html)' if html else 'Plain Text (*.txt)')
        if filename: # got!
            try:
                formatter = strutil.NameLoader(application.namefile).conv if self.cfparser.config.getboolean(self.cfparser.section, 'show_names') else str
                with open(filename, mode='w', encoding=sys.getdefaultencoding()) as file:
                    if html:
                        strutil.print_form_html(table=self.table.getSeatList(formatter=formatter),
                                                file=file,
                                                **self.rmdhdcfg(head='html_', ignore=('txt_', 'save_as_html', 'show_names')))
                    else:
                        strutil.print_form_txt(table=self.table.getSeatList(formatter=formatter),
                                               file=file,
                                               **self.rmdhdcfg(head='txt_', ignore=('html_', 'save_as_html', 'show_names'), cot=True))
            except Exception:
                exc = traceback.format_exc()
                msgbox = QMessageBox()
                msgbox.setWindowTitle('錯誤')
                msgbox.setIcon(msgbox.Warning)
                msgbox.setText('存檔期間發生錯誤\n\n'+exc.splitlines()[-1])
                msgbox.setDetailedText(exc)
                msgbox.addButton('確定', msgbox.AcceptRole)
                msgbox.exec_()
            else:
                self.seatIsSaved = True
                msgbox = QMessageBox()
                msgbox.setWindowTitle('存檔成功')
                msgbox.setIcon(msgbox.Information)
                msgbox.setText('已成功將檔案儲存至：\n'+
                               filename)
                msgbox.addButton('確定', msgbox.AcceptRole)
                msgbox.addButton('開啟檔案', msgbox.ApplyRole).clicked.connect(lambda: defaultOpen(filename))
                msgbox.exec_()
                
    def meth_saveOptions_clicked(self):
        m = MWSaveOptionsDialog(parent=self)
        m.exec_()
        
    def meth_aboutButton_clicked(self):
        QMessageBox.about(self,
                          '關於'+application.appname+' '+str(__version__),
                          '''\
作者
{author}

版本
excseat.ui_pyside {version}
excseat.core {corever}

執行環境
Python {pyv[0]}.{pyv[1]}.{pyv[2]}
PySide {pysidever}
Qt {qtver}\
'''.format(author=application.author,
           version=__version__,
           corever=core.__version__,
           pyv=sys.version_info,
           pysidever=PySide.__version__,
           qtver=PySide.QtCore.__version__))
         

def main():
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon(application.icon))
    os.makedirs(application.userdata, exist_ok=True)
    open(application.namefile, 'a').close()
    config = Configure()
    config.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
    