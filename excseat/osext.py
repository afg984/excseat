'''
Created on Jun 15, 2013

@author: afg
'''

import sys
import os
from subprocess import call

def defaultOpen(path):
    if sys.platform == 'win32':
        os.startfile(path)
    elif sys.platform == 'darwin':
        call(['open', path])
    else:
        try:
            call(['xdg-open', path])
        except:
            pass