'''
Created on Jul 21, 2013

@author: afg
'''

import sys
import locale
import string
import unicodedata

WIDTH_MAPPING = dict(F=2,
                     W=2,
                     H=1,
                     N=1,
                     Na=1)
LJUST = 'left'
RJUST = 'right'
CENTER = 'center'

class NameLoader:
    def __init__(self, filename, **opcf):
        self.namemap = dict()
        for line in self.read(filename).splitlines():
            try:
                number, name = line.strip().split(' ', 1)
            except ValueError:
                pass
            else:
                if number.isdigit():
                    self.namemap[number] = name
                        
    def conv(self, number):
        try:
            return '{number}\n{name}'.format(number=number, name=self.namemap[str(number)])
        except KeyError:
            return str(number)
    
    @staticmethod
    def read(filename, codecs=[locale.getpreferredencoding(), sys.getdefaultencoding(), 'utf-8']):
        for codec in codecs:
            try:
                with open(filename, encoding=codec) as file:
                    return file.read()
            except UnicodeDecodeError:
                pass
        raise UnicodeDecodeError('None of the codecs can decode the file:', filename) 

def int_to_chars(num):
    ret = ''
    while num > 0:
        num, new = divmod(num-1, len(string.ascii_uppercase))
        ret = string.ascii_uppercase[new] + ret
    return ret

def swidth(s):
    result = 0
    for c in s:
        w = unicodedata.east_asian_width(c)
        if w == 'A':
            # ambiguous
            raise ValueError("ambiguous character %x" % (ord(c)))
        result += WIDTH_MAPPING[w]
    return result

def valign(string, column_width, fillchar=' ', alignment=CENTER):
    if swidth(fillchar) != 1:
        raise TypeError('fillchar\'s column width should be 1 instead of %d' % swidth(fillchar))
    i = 0
    width = 0
    for c in string:
        try:
            w = swidth(c)
        except ValueError:
            return None
        else:
            width += w
        i += 1
        if width == column_width:
            return string[:i]
        if width > column_width:
            return string[:i-1]+fillchar
    fcs = column_width - width
    if alignment == LJUST:
        return string + fillchar * fcs
    elif alignment == RJUST:
        return fillchar * fcs + string
    elif alignment == CENTER:
        return fillchar*(fcs//2) + string + fillchar*(fcs-fcs//2)
    else:
        raise ValueError('Unknown alignment: '+repr(alignment))

def hcenter(block, line, height):
    line -= (height-block.count('\n')-1)//2
    if 0 <= line <= block.count('\n'):
        return block.split('\n')[line]
    else:
        return ''
    
def addsn(table, quote=lambda s: s):
    table[:0] = [[quote(int_to_chars(x)) for x in range(len(table[0])+1)]]
    for rid, row in enumerate(table):
        if rid:
            row[:0] = [quote(str(rid))]

def print_form_txt(table, show_line_numbers=True, same_cell_size=False, borderless=False,
                    hexpand=2, vexpand=0, alignment=CENTER, file=sys.stdout):
    def write(*args, **kwargs):
        print(*args, file=file, **kwargs)
    if show_line_numbers:
        addsn(table, quote=lambda s: '['+s+']' if s else '')
    row_height = [max(b.count('\n')+vexpand for b in row)+1 for row in table]
    column_width = [max(max(swidth(line)+hexpand for line in b.split('\n')) for b in column) for column in zip(*table)]
    if same_cell_size:
        row_height = [max(row_height)] * len(row_height)
        column_width = [max(column_width)] * len(column_width)
    # OUTPUT
    if not borderless:
        write('+', end='')
        for hborder in column_width:
            write('-'*hborder, end='+')
        write()
    for row_index, row in enumerate(table):
        for line_index in range(row_height[row_index]):
            if not borderless:
                write('|', end='')
            for column_index, block in enumerate(row):
                write(valign(hcenter(block, line_index, row_height[row_index]),
                            column_width[column_index],
                            alignment=alignment),
                      end='' if borderless else'|')
            write()
        if not borderless:
            write('+', end='')
            for hborder in column_width:
                write('-'*hborder, end='+')
            write()

def print_form_html(table, show_line_numbers=True, alignment=CENTER, vpadding=8, hpadding=12, border=3,
                    font_size=20, seat_color='DimGray', file=sys.stdout):
    addsn(table)
    def write(*args):
        print(*args, sep='', end='', file=file)
    write('<!DOCTYPE html>')
    write("<meta charset='utf-8'>")
    write('<html>', '<head>')
    write('<style>')
    write('body', '{font-family:"Droid Sans","Verdana","Arial";font-size:', font_size, 'px;}')
    write('table', '{text-align:', alignment, ';table-layout:fixed;}')
    write('td', '{padding:', vpadding, 'px ', hpadding,'px;background-color:black;}')
    write('td.sd', '{border:', border, 'px solid white;background-color:', seat_color,';color:white;}')
    write('td.hd', '{color:', seat_color, ';background-color:white;}')
    write('td.bk', '{background-color:white;}')
    write('</style>')
    write('</head>', '<body>')
    write('<table>')
    for rid, row in enumerate(table):
        write('<tr>')
        for cid, block in enumerate(row):
            if show_line_numbers and not (rid and cid):
                cls = 'hd'
            elif block:
                cls = 'sd'
            else:
                cls = 'bk'
            write('<td class="{cls}">{block}</td>'.format(cls=cls, block=block.replace('\n', '<br>')))
        write('</tr>')
    write('</table>')
    write('</body></html>')
        
