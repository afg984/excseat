'''
Created on Jun 11, 2013

@author: afg
'''

import random
from collections import deque

__version__ = '0.2.0'

class Seat:
    DISABLED_LAYOUT = ''
    def __init__(self, onseat=None, disabled=False):
        self._onseat = onseat
        self._disabled = disabled
        
    def __repr__(self):
        return '{1}Seat({0})'.format('blank' if self._onseat is None else self._onseat,
                                     'disabled-' if self._disabled else '')
    def __str__(self):
        return '' if self._onseat is None else self.DISABLED_LAYOUT if self.isDisabled() else str(self._onseat)
    
    def reset(self):
        self._onseat = None
        self._disabled = False
        self.DISABLED_LAYOUT = Seat.DISABLED_LAYOUT
    
    def isAvailable(self):
        return not self.isDisabled() and self.isBlank()
            
    def isBlank(self):
        return self._onseat is None
        
    def isDisabled(self):
        return self._disabled
        
    def disable(self, m=True):
        if self._onseat is not None:
            raise SeatError('Seat must be blank to disable')
        self._disabled = m
        
    def seat(self, person):
        if self._disabled:
            raise SeatError('This seat is disabled')
        ret = self._onseat
        self._onseat = person
        return ret
    
    def kick(self):
        if self._disabled:
            raise SeatError('This seat is disabled')
        ret = self._onseat
        self._onseat = None
        return ret
        
        
class SeatError(Exception):
    pass


class Table:
    STATUS_DISABLE = 1
    STATUS_CHOOSE = 2
    STATUS_DONE = 3
    
    def __init__(self, rows, cols, nums):
        if not len(nums):
            raise TableError('No people to choose seats')
        if not rows > 0:
            raise TableError('Invalid row quantity')
        if not cols > 0:
            raise TableError('Invalid column quantity')
        if rows * cols < len(nums):
            raise TableError('People more than seats')
        self._rows = rows
        self._cols = cols
        self._nums = tuple(nums)
        self._seats = [[Seat() for col in range(self._cols)] for row in range(self._rows)]
        self.reset()
    
    def __repr__(self):
        return '<{}x{} Table>'.format(self._rows, self._cols)
        
    def reset(self):
        self._queue = deque(self._nums)
        random.shuffle(self._queue)
        for row in self._seats:
            for seat in row:
                seat.reset()
        self._skipped_disable = False
        self._undos = []
        
    def getQueue(self):
        return tuple(self._queue)
        
    def getStatus(self):
        if [seat._disabled for row in self._seats for seat in row].count(False) > len(self._nums) and not self._skipped_disable:
            return self.STATUS_DISABLE
        elif self._queue:
            return self.STATUS_CHOOSE
        else:
            return self.STATUS_DONE
        
    def getSeat(self, row, col):
        return self._seats[row][col]
    
    def getAll(self):
        return [(row, col) for row in range(self._rows) for col in range(self._cols)]
    
    def getAvailable(self):
        return [loc for loc in self.getAll() if self.getSeat(*loc).isAvailable()]
    
    def getSeated(self):
        return [loc for loc in self.getAll() if self.getSeat(*loc)._onseat is not None]
        
    def getSeatList(self, formatter=str):
        return [[formatter(block) for block in row] for row in self._seats]
            
    def action_skip_disable(self, m=True):
        if self.getStatus() != self.STATUS_DISABLE:
            raise TableError('can only disable')
        self._register_undo(self._skip_disable, self._skipped_disable)
        self._skip_disable(m)
        
    def action_toggle(self, row, col):
        if self.getStatus() is self.STATUS_DISABLE:
            self._register_undo(self.getSeat(row, col).disable, self.getSeat(row, col).isDisabled())
            self._seats[row][col].disable(not self._seats[row][col].isDisabled())
        elif self.getStatus() is self.STATUS_CHOOSE:
            ponseat = self.getSeat(row, col).seat(self._queue[0])
            if ponseat is None:
                self._register_undo(self._back_to_queue, row, col)
                self._queue.popleft()
            else:
                def _undo(row, col, ponseat):
                    self._queue.popleft()
                    self._back_to_queue(row, col)
                    self.getSeat(row, col).seat(ponseat)
                self._register_undo(_undo, row, col, ponseat)
                self._queue[0] = ponseat
        elif self.getStatus() is self.STATUS_DONE:
            ponseat = self.getSeat(row, col).kick()
            if ponseat is None:
                pass
            else:
                self._register_undo(self._goto_seat, row, col)
                self._queue.appendleft(ponseat)
        else:
            raise TableError('Internal Error: unknown status')
                
    def action_postpone(self):
        if self.getStatus() == self.STATUS_CHOOSE:
            self._register_undo(lambda: self._queue.appendleft(self._queue.pop()))
            self._queue.append(self._queue.popleft())
        else:
            raise TableError('Can postpone only in STATUS_CHOOSE')
    
    def action_random(self):
        if self.getStatus() != self.STATUS_CHOOSE:
            raise TableError('action_random only valid in STATUS_CHOOSE')
        choice = random.choice(self.getAvailable())
        self._register_undo(self._back_to_queue, *choice)
        return self._goto_seat(*choice)
        
    def action_random_all(self):
        if self.getStatus() != self.STATUS_CHOOSE:
            raise TableError('action_random_all only valid in STATUS_CHOOSE')
        available = self.getAvailable()
        old_available = available[:]
        old_queue = deque(self._queue)
        random.shuffle(available)
        def _undo(seat_cleanup, old_queue):
            for row, col in seat_cleanup:
                self.getSeat(row, col).kick()
            self._queue = old_queue
        while self._queue:
            self._goto_seat(*available.pop())
        self._register_undo(_undo,
                           [loc for loc in old_available if loc not in available],
                           old_queue)
            
    def action_undo(self):
        m = self._undos.pop()
        m[0](*m[1], **m[2])
        
    def _skip_disable(self, m):
        self._skipped_disable = m
        
    def _back_to_queue(self, row, col):
        self._queue.appendleft(self.getSeat(row, col).kick())
        
    def _goto_seat(self, row, col):
        if self.getStatus() is not self.STATUS_CHOOSE:
            raise TableError('Status should be STATUS_CHOOSE to _goto_seat')
        try:
            pop_protect = self._queue.popleft()
        except IndexError:
            raise TableError('Queue is empty')
        else:
            try:
                return self._seats[row][col].seat(pop_protect)
            except SeatError:
                self._queue.appendleft(pop_protect)
                raise
            
    def _register_undo(self, func, *args, **kwargs):
        self._undos.append((func, args, kwargs))

class TableError(Exception):
    pass

    