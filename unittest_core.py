'''
Created on Jun 13, 2013

@author: afg
'''

import unittest
from excseat import core, strutil
import random
import string

class Test(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None
    
    def test_seat(self):
        'logic test'
        
        # SETUP
        seat = core.Seat()
        
        # INITIAL STATE
        self.assertEqual(seat.isDisabled(), False)
        self.assertEqual(seat.isBlank(), True)
        self.assertEqual(seat.isAvailable(), True)
        
        # DISABLING
        seat.disable()
        self.assertEqual(seat.isDisabled(), True)
        self.assertEqual(seat.isBlank(), True)
        self.assertEqual(seat.isAvailable(), False)
        # DO NOT ALLOW SEATING WHEN DISABLED
        self.assertRaises(core.SeatError, seat.seat, (1))
        
        # ENABLING
        seat.disable(False)
        self.assertEqual(seat.isDisabled(), False)
        self.assertEqual(seat.isBlank(), True)
        self.assertEqual(seat.isAvailable(), True)
        
        # SEAT IN
        seat.seat(1)
        self.assertEqual(seat.isDisabled(), False)
        self.assertEqual(seat.isBlank(), False)
        self.assertEqual(seat.isAvailable(), False)
        # DO NOT ALLOW DISABLE 
        self.assertRaises(core.SeatError, seat.disable)
        
        # SEAT & KICK PERSON RETURN CURRENT PERSON ON SEAT
        self.assertEqual(seat.seat(2), 1)
        self.assertEqual(seat.kick(), 2)
        
        # SEAT SHOUD BE CLEAN
        self.assertEqual(seat.isDisabled(), False)
        self.assertEqual(seat.isBlank(), True)
        self.assertEqual(seat.isAvailable(), True)
    
    def test_table_simple(self):
        'TESTS: Logic & Exception'
        
        # SIMPLE TEST: SHOULD RAISE ERROR IF NO ARGS
        self.assertRaises(TypeError, core.Table)
        # RAISE WHEN NO PEOPLE SENT
        self.assertRaises(core.TableError, core.Table, 5, 5, [])
        # SIMPLE TEST: RAISE WHEN PEOPLE MORE THAN SEATS
        self.assertRaises(core.TableError, core.Table, 5, 5, list(range(26)))
        # SIMPLE TEST: RAISE WHEN ROW OR COL COUNT INVALID
        self.assertRaises(core.TableError, core.Table, 0, 5, list(range(10)))
        self.assertRaises(core.TableError, core.Table, 5, 0, list(range(10)))
        
        # SETUP
        rows = 6
        columns = 7
        allseats = [(row, col) for row in range(rows) for col in range(columns)]
        nums = [x for x in range(1, 41) if x not in (29, 30, 36)]
        table = core.Table(rows, columns, nums)
        
        # SIMPLE TEST: GET SEAT
        for row, col in allseats:
            self.assertIs(table._seats[row][col], table.getSeat(row, col))
        # SIMPLE TEST: RESET SHOULD NOT CHANGE REFERENCE
        table.reset()
        for row, col in allseats:
            self.assertIs(table._seats[row][col], table.getSeat(row, col))
        
        original_queue = list(table._queue)
        
        # SIMPLE TEST: STATUS
        self.assertEqual(table.getStatus(), table.STATUS_DISABLE)
        table.action_skip_disable()
        self.assertEqual(table.getStatus(), table.STATUS_CHOOSE)
        
        # SIMPLE TEST: SHOULD FINISH DISABLE FIRST
        table.action_undo() # AKA table._skip_disable(False); table._undos.pop()
        self.assertEqual(table.getStatus(), table.STATUS_DISABLE)
        self.assertRaises(core.TableError, table._goto_seat, *random.choice(allseats))
        #self.assertRaises(core.TableError, table.swap, random.choice(allseats), random.choice(allseats)) swap removed since 2.0.8
        self.assertRaises(core.TableError, table.action_postpone)
        self.assertRaises(core.TableError, table.action_random)
        self.assertRaises(core.TableError, table.action_random_all)
        
        # SIMPLE TEST: GETTERS
        self.assertEqual(table.getQueue(), tuple(table._queue))
        self.assertEqual(table.getAll(), allseats)
        available = [(x, y) for x in range(rows) for y in range(columns)]
        self.assertEqual(table.getAvailable(), available)
        
        # TEST: DISABLE A SEAT
        random.shuffle(available)
        removed = available.pop()
        table.action_toggle(*removed)
        self.assertEqual(table.getSeat(*removed).isDisabled(), True)
        self.assertEqual(table.getAvailable(), sorted(available))
        
        # TEST: ENABLE A SEAT
        available.append(removed)
        table.action_toggle(*removed)
        self.assertEqual(table.getSeat(*removed).isDisabled(), False)
        self.assertEqual(table.getAvailable(), sorted(available))
        
        # TEST: DISABLE SEATS
        random.shuffle(available)
        self.assertEqual(table.getStatus(), table.STATUS_DISABLE)
        while table.getStatus() == table.STATUS_DISABLE:
            removed = available.pop()
            table.action_toggle(*removed)
            self.assertEqual(table.getSeat(*removed).isDisabled(), True)
            self.assertEqual(table.getAvailable(), sorted(available))
            # QUEUE SHOULD BE THE SAME
            self.assertEqual(list(table._queue), original_queue)
            
        # TEST: DISABLED SEATS
        for loc in table.getAll():
            if loc not in available:
                self.assertRaises(core.SeatError, table._goto_seat, *loc)
        # QUEUE SHOULD BE THE SAME
        self.assertEqual(list(table._queue), original_queue)
            
        # TEST: SEAT IN SEATS
        seated = dict()
        on_seat = list()
        self.assertEqual(table.getStatus(), table.STATUS_CHOOSE)
        while table.getStatus() == table.STATUS_CHOOSE:
            loc = available.pop()
            on_seat.append(loc)
            seated[loc] = table._queue[0]
            table.action_toggle(*loc)
            self.assertEqual(table.getAvailable(), sorted(available))
            # CHECK EVERY SEAT
            for row in range(rows):
                for col in range(columns):
                    try:
                        onseat = seated[(row, col)]
                    except KeyError:
                        onseat = None
                    self.assertEqual(table.getSeat(row, col)._onseat, onseat)
        # SIMPLE TEST: STATUS DONE
        self.assertEqual(table.getStatus(), table.STATUS_DONE)
        # EXCEPTION TEST
        self.assertRaises(core.TableError, table.action_postpone)
        self.assertRaises(core.TableError, table.action_random)
        self.assertRaises(core.TableError, table.action_random_all)
        self.assertRaises(core.TableError, table.action_skip_disable)
        
        # TEST UNDO
        while on_seat:
            table.action_undo()
            on_seat.pop()
            for row, col in table.getAll():
                if (row, col) in on_seat:
                    self.assertEqual(table.getSeat(row, col)._onseat, seated[(row, col)])
                else:
                    self.assertEqual(table.getSeat(row, col)._onseat, None)
                    
        self.assertEqual(table.getStatus(), table.STATUS_CHOOSE)
        # QUEUE SHOULD BE SAME AS ORIGINAL
        self.assertEqual(list(table._queue), original_queue)
        
        # UNDO DISABLE
        for x in range(len(allseats)-len(nums)):
            table.action_undo()
        self.assertEqual(table.getAll(), table.getAvailable())
    
    @staticmethod
    def getTableStatus(table):
        'GET TABLE STATUS FOR TEST'
        return dict(queue=table.getQueue(),
                    seats=[(table.getSeat(*loc)._onseat,
                            table.getSeat(*loc).isDisabled()) for loc in table.getAll()],
                            status=table.getStatus(),
                            available=table.getAvailable(),
                            skipped_disable=table._skipped_disable)
    
    def test_table_postpone(self):
        'TESTS: choose, postpone, undo'
        rows = random.randint(10, 20)
        cols = random.randint(10, 20)
        t = core.Table(rows, cols, range(rows * cols))
        
        watch = list()
        
        while t.getStatus() != t.STATUS_DONE:
            watch.append(self.getTableStatus(t))
            if random.randrange(5):
                t.action_toggle(*random.choice(t.getAvailable()))
            else:
                t.action_postpone()
                
        while watch:
            t.action_undo()
            self.assertEqual(self.getTableStatus(t), watch.pop())
    
    def test_table_random_undo(self):
        'TESTS: disable, skip_disable, random, random_all, undo'
        
        t = core.Table(10, 10, range(80))
        
        # DISABLE SEATS
        available = t.getAvailable()
        random.shuffle(available)
        for x in range(20):
            t.action_toggle(*available.pop())
        
        # STATUS CHOOSE
        self.assertEqual(t.getStatus(), t.STATUS_CHOOSE)
        
        watch = list()
        
        # ACTION_RANDOM
        for x in range(40):
            watch.append(self.getTableStatus(t))
            t.action_random()
        
        # ACTION_RANDOM_ALL
        watch.append(self.getTableStatus(t))
        t.action_random_all()
        
        # STATUS DONE
        self.assertEqual(t.getStatus(), t.STATUS_DONE)
        
        # UNDO CHECK
        while watch:
            t.action_undo()
            self.assertEqual(self.getTableStatus(t), watch.pop())
        
    def test_table_swap(self):
        'TESTS: choose, swap, postpone'
        t = core.Table(4, 4, range(16))
        m = (random.randrange(4), random.randrange(4))
        
        p_a, p_b, p_c = t.getQueue()[:3]
        tp_queue = list(t.getQueue())
        
        t.action_toggle(*m)
        self.assertEqual(t.getSeat(*m)._onseat, p_a)
        
        t.action_toggle(*m)
        self.assertEqual(t.getSeat(*m)._onseat, p_b)
        self.assertEqual(t.getQueue()[0], p_a)
        
        tp_queue.pop(1)
        tp_queue.append(tp_queue.pop(0))
        
        t.action_postpone()
        self.assertEqual(t.getQueue(), tuple(tp_queue))
        
        t.action_toggle(*m)
        tp_queue[0] = p_b
        self.assertEqual(t.getSeat(*m)._onseat, p_c)
        self.assertEqual(t.getQueue(), tuple(tp_queue))
        
    def test_z_table_full(self):
        'TESTS: disable, skip_disable, choose, random, random_all, postpone, swap, undo'
        def showStatus(operation='', stage='', pending='', steps='', end='\r'):
            print(' [{operation:^4}] {stage:>10} {pending:>10} {steps:>10}'.format(operation=operation,
                                                                                   stage=stage,
                                                                                   pending=pending,
                                                                                   steps=steps),
                  end=end)
            
        rows = random.randint(20, 50)
        cols = random.randint(20, 50)
        nums = list(range(random.randint(rows*cols//2, rows*cols)))
        table = core.Table(rows, cols, nums)
        watch = list()
        
        print()
        print('='*70)
        print(' TABLE FULL TEST, {}, POP={}'.format(table, len(nums)))
        print('='*70)
        
        showStatus('OPER', 'STAGE', 'PENDING', 'STEPS', end='\n')
        
        # STAGE DISABLE
        showStatus('DO', 'DISABLE', '-', '-')
        while table.getStatus() == table.STATUS_DISABLE:
            watch.append(self.getTableStatus(table))
            if random.randrange(rows*cols-len(nums)):
                table.action_toggle(*random.choice(table.getAvailable()))
            else:
                table.action_skip_disable()  
            showStatus('DO', 'DISABLE', len(table.getAvailable())-len(nums), len(watch))
        print()
        
        self.assertEqual(len(watch), len(table._undos),
                         'length of undo and watch should be the same')
        
        # STAGE CHOOSE
        nsp = len(watch)
        people_count = len(nums)
        while table.getStatus() == table.STATUS_CHOOSE:
            watch.append(self.getTableStatus(table))
            action = random.randrange(10)
            if action == 0 and table.getSeated():
                table.action_toggle(*random.choice(table.getSeated()))
            elif action == 1:
                table.action_postpone()
            elif not random.randrange(people_count):
                table.action_random_all()
            elif random.randrange(1):
                table.action_random()
            else:
                table.action_toggle(*random.choice(table.getAvailable()))
            showStatus('DO', 'CHOOSE', len(table._queue), len(watch)-nsp)
        print()
        
        self.assertEqual(len(watch), len(table._undos),
                         'length of undo and watch should be the same')
        self.assertEqual(table.getStatus(), table.STATUS_DONE)
        
        # STAGE DONE-SWAP
        showStatus('DO', 'DONE-SWAP', '-', '-')
        nsp = len(watch)
        done_swaps = random.randrange(500)
        for x in range(done_swaps):
            watch.append(self.getTableStatus(table))
            table.action_toggle(*random.choice(table.getSeated()))
            showStatus('DO', 'DONE-SWAP', done_swaps-x-1, len(watch)-nsp)
        print()
        
        self.assertEqual(len(watch), len(table._undos),
                         'length of undo and watch should be the same')
        
        # STAGE UNDO CHECK
        cs = len(watch)
        while watch:
            table.action_undo()
            self.assertEqual(self.getTableStatus(table), watch.pop())
            showStatus('UNDO', 'CHECK', len(watch), cs-len(watch))
        print()
        
    def test_int_to_chars(self):
        for x in range(100):
            num = random.randrange(x**x)
            cvdstr = strutil.int_to_chars(num)
            self.assertEqual(num, sum((string.ascii_uppercase.find(char)+1)*len(string.ascii_uppercase)**digit for (digit, char) in enumerate(reversed(cvdstr))))
            #print(cvdstr, 'is', num)  
        
        
if __name__ == '__main__':
    unittest.main()
    